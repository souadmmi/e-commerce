package co.ecommerce.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Commande;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Integer> {
}